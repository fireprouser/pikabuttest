import UIKit

protocol PostView {
    func showPost(post: DataSource<PostCell>)
}

protocol ImageLoader {
    func loadImage(with url: URL, completion: @escaping ((UIImage?) -> ()))
}

class PostPresenter: PostFetcher {
    let postProvider: PostProvider
    let postView: PostView

    init(with provider: PostProvider, view: PostView) {
        self.postProvider = provider
        self.postView = view
    }

    func fetch(with postID: Int) {
        self.postProvider.loadPost(with: postID) { (postItem) in
            let postItems = postItem.flatMap({PostPresenter.convertToViewModel($0)}) ?? []
            let posts = self.toPostCell(rows: postItems)
            let section = Section<PostCell>(items: posts)
            self.postView.showPost(post: section.items.isEmpty ? DataSource(sections: []) : DataSource(sections: [section]))
        }
    }


    private func toPostCell(rows: [RowViewModel]) -> [PostCell] {
        return rows.map { (viewModel) -> PostCell? in
            if let imageViewModel = viewModel as? ImageViewModel {
                return .imageCell(imageViewModel)
            } else if let postViewModel = viewModel as? PostViewModel {
                return .postCell(postViewModel)
            }
            return nil

        }.compactMap({$0})
    }

    deinit {
        print("Deinit")
    }
}

extension PostPresenter {
    static func convertToViewModel(_ post: PostItem) -> [RowViewModel] {

        let imagesViewModel = post.images.compactMap { (url) -> ImageViewModel? in
            return URL(string: url).flatMap({ImageViewModel(url: $0, image: Observable<UIImage?>(nil))})
        }

        return [PostViewModel(title: post.title, text: post.text)] + imagesViewModel
    }
}


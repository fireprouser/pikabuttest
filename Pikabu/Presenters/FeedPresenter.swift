import Foundation

protocol FeedView {
    func showFeed(feed: DataSource<FeedViewModel>)
}

class FeedPresenter {
    let feedProvider: FeedProvider
    let feedView: FeedView

    init(with provider: FeedProvider, view: FeedView) {
        self.feedProvider = provider
        self.feedView = view
    }

    func fetch() {
        self.feedProvider.provideFeed { (items) in
            let feedViewModels = Section<FeedViewModel>(items: items.map({FeedViewModel(id: $0.id, title: $0.title, preview: $0.preview)}))
            self.feedView.showFeed(feed: DataSource<FeedViewModel>(sections: [feedViewModels]))
        }
    }
}

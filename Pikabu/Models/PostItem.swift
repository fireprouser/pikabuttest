import Foundation

struct Post: Codable, Equatable {
    let post: PostItem
}

struct PostItem: Codable, Equatable {
    let id: Int
    let timestamp: Int
    let title: String
    let text: String
    let likesCount: Int
    let images: [String]

    enum CodingKeys: String, CodingKey {
        case id = "postId"
        case title = "title"
        case timestamp = "timeshamp"
        case text = "text"
        case likesCount = "likes_count"
        case images = "images"
    }
}

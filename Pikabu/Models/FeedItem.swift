import Foundation

struct Feed: Codable {
    let posts: [FeedItem]
}

struct FeedItem: Codable, Equatable {
    let id: Int
    let timestamp: Int
    let title: String
    let preview: String
    let likesCount: Int

    enum CodingKeys: String, CodingKey {
        case id = "postId"
        case title = "title"
        case timestamp = "timeshamp"
        case preview = "preview_text"
        case likesCount = "likes_count"
    }
}

import Foundation

struct FeedViewModel {
    let id: Int
    let title: String
    let preview: String
}

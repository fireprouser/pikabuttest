import UIKit

protocol RowViewModel {}

struct PostViewModel: RowViewModel {
    let title: String
    let text: String
}

struct ImageViewModel: RowViewModel {
    let url: URL
    let image: Observable<UIImage?>
}

enum PlaceHolderImage {
    case placeHolder, error
}

extension PlaceHolderImage {
    var image: UIImage {
        switch self {
        case .placeHolder:
            return UIImage(named: "placeholder")!
        case .error:
            return UIImage(named: "error")!
        }
    }

}


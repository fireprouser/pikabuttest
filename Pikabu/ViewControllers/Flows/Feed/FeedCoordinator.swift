import UIKit

class FeedCoordinator: BaseCoordinator, FeedCoordinatorOutput {
    private let router: Router
    private let feedViewControllerFactory: FeedViewControllerFactory
    private let postViewControllerFactory: PostViewControllerFactory

    var didEndFlow: (() -> ())?

    init(router: Router, feedViewControllerFactory: FeedViewControllerFactory, postViewControllerFactory: PostViewControllerFactory) {
        self.router = router
        self.feedViewControllerFactory = feedViewControllerFactory
        self.postViewControllerFactory = postViewControllerFactory
    }

    override func start() {
        self.showFeed()
    }

    override func start(with options: DeepLinkOption?) {
        if let option = options {
            switch option {
            case .post(let id):
                DispatchQueue.main.async {
                    self.showPost(feedId: id)
                }
            }
        }
    }

    private func showFeed() {
        var (feedView, feedPresenter) = self.feedViewControllerFactory.feedViewCoontroller(with: URLSession.shared)
        feedView.clickOnFeed = { [weak self] item in
            self?.showPost(feedId: item.id)
        }

        feedView.reloadData = {
             feedPresenter.fetch()
        }

        self.router.setRootModule(feedView, hideBar: false)
    }

    func showPost(feedId: Int) {
        var (view, fetcher) = self.postViewControllerFactory.postViewCoontroller(with: URLSession.shared, post: feedId)

        view.reloadData = {
            fetcher.fetch(with: feedId)
        }

        self.router.push(view, animated: true, hideBottomBar: false, completion: nil)
    }
}

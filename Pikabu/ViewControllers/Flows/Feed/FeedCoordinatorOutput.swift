import Foundation

protocol FeedCoordinatorOutput {
    var didEndFlow: (()->())? { get set }
}

import UIKit

class FeedViewController: EmbeddedTableViewController<Configurator<FeedCellView, FeedViewModel>>, FeedViewOutput, FeedViewInput  {
    var clickOnFeed: ((FeedViewModel) -> ())?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadData?()
        self.navigationController?.navigationBar.isHidden = true
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.clickOnFeed?(self.dataSource.value.item(for: indexPath))
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension FeedViewController: FeedView {
    func showFeed(feed: DataSource<FeedViewModel>) {
        self.dataSource.value = feed
    }
}


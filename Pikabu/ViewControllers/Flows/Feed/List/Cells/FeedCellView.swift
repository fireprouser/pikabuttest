import UIKit

class FeedCellView: UITableViewCell {
    private struct FeedViewCellConstants {
        static let readMore = "Читать далее"
        static let collapse = "Скрыть"
        static let maxCountOfPresentingLine = 2
    }

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var previewLabel: UILabel!
    @IBOutlet private weak var readMore: UIButton!

    var expandTouched: (() -> ())?

    func configure(configure: FeedViewModel) {
        self.titleLabel.text = configure.title
        self.previewLabel.text = configure.preview
        self.readMore.isHidden = !self.previewLabel.isTruncated && self.previewLabel.numberOfLines != 0
    }

    @IBAction func readMoreTouched(_ sender: Any) {
        let expended = self.previewLabel.numberOfLines == 0

        self.previewLabel.numberOfLines = expended ? FeedViewCellConstants.maxCountOfPresentingLine : 0

        self.readMore.setAttributedTitle(expended ?
            NSAttributedString(string: FeedViewCellConstants.readMore) :
            NSAttributedString(string: FeedViewCellConstants.collapse), for: .normal)

        self.expandTouched?()
    }
}

extension UILabel {
      var isTruncated: Bool {
          guard let labelText = text else {
              return false
          }

          let labelTextSize = (labelText as NSString).boundingRect(
              with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
              options: .usesLineFragmentOrigin,
              attributes: [.font: font as Any],
              context: nil).size

          return labelTextSize.height > bounds.size.height
      }
}

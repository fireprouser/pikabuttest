//
//  ImageViewCell.swift
//  Pikabu
//
//  Created by Dmytro on 11/16/19.
//  Copyright © 2019 ReliableSoftwareImp. All rights reserved.
//

import UIKit

class ImageViewCell: UITableViewCell {
    @IBOutlet private weak var postImage: UIImageView!
    
    var viewModel: ImageViewModel?

    override func prepareForReuse() {
        self.viewModel?.image.valueChanged = nil
    }
    
    func setup(with viewModel: ImageViewModel) {
        self.postImage.image = nil
        self.viewModel = viewModel
        
        if let image = viewModel.image.value {
            self.postImage.image = image
            return
        }

        self.viewModel?.image.valueChanged = {[weak self] image in
            DispatchQueue.main.async {
                self?.postImage?.image = image
            }
        }
    }
}

extension UIImage {
    func widthRatio() -> CGFloat {
        return self.size.width / self.size.height
    }
}

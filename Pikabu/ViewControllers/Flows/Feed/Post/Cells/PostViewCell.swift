//
//  PostViewCell.swift
//  Pikabu
//
//  Created by Dmytro on 11/16/19.
//  Copyright © 2019 ReliableSoftwareImp. All rights reserved.
//

import UIKit

class PostViewCell: UITableViewCell {
    @IBOutlet private weak var postTitle: UILabel!
    @IBOutlet private weak var postText: UILabel!
    
    func setup(with viewModel: PostViewModel) {
        self.postTitle.text = viewModel.title
        self.postText.text = viewModel.text
    }
}

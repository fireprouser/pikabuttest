import UIKit

class PostTableViewController: EmbeddedTableViewController<CompositeConfigurator>, PostViewOutput {

    var viewDidFinished:(() -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.allowsMultipleSelection = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadData?()
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewDidFinished?()
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.dataSource.value.item(for: indexPath) {
        case .imageCell(let model):
            if let imageSize = model.image.value {
                let size = self.tableView.frame.size.width / imageSize.widthRatio()
                return size
            }
        default:
             return UITableView.automaticDimension
        }
        return UITableView.automaticDimension
    }
}

extension PostTableViewController: PostView {
    func showPost(post: DataSource<PostCell>) {
        self.dataSource.value = post
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
}

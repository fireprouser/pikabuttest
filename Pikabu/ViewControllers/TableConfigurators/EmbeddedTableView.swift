import UIKit

class EmbeddedTableViewController<TableCellConfigurator: CellConfigurator>: UITableViewController {

    let dataSource: Observable<DataSource<TableCellConfigurator.Model>>
    let configurator: TableCellConfigurator

    var reloadData: (() -> ())?

    init(dataSource: Observable<DataSource<TableCellConfigurator.Model>>, configurator: TableCellConfigurator) {
        self.dataSource = dataSource
        self.configurator = configurator
        super.init(nibName: nil, bundle: nil)
        configurator.registerCells(in: tableView)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { fatalError() }

    override func viewDidLoad() {
        super.viewDidLoad()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:  #selector(fetchData), for: .valueChanged)
        self.refreshControl = refreshControl
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        dataSource.valueChanged = {[weak self] value in
            DispatchQueue.main.async {
                if value.sectionCount() == 0 {
                    self?.showEmptyState()
                } else {
                    self?.tableView.reloadData()
                    self?.hideEmptyState()
                }
                self?.refreshControl?.endRefreshing()
            }
        }

        self.tableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        dataSource.valueChanged = nil
    }

    @objc func fetchData() {
        self.reloadData?()
    }

    func showEmptyState() {
        let view = UIView(frame: tableView.bounds)
        tableView.backgroundView = view
        let text = UILabel()
        text.translatesAutoresizingMaskIntoConstraints = false
        text.text = "Ooops, something went wrong."
        text.numberOfLines = 0
        self.tableView.backgroundView?.addSubview(text)
        let contstraintX = NSLayoutConstraint(item: text, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let contstraintY = NSLayoutConstraint(item: text, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([contstraintX, contstraintY])
    }

    func hideEmptyState() {
        self.tableView.backgroundView = nil
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.value.sectionCount()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.value.countItems(in: section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource.value.item(for: indexPath)
        return configurator.cellFor(model: model, indexPath: indexPath, tableView: tableView)
    }
}

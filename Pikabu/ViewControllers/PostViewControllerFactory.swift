import UIKit

protocol PostViewControllerFactory {
    func postViewCoontroller(with loader: Loader, post ID: Int) -> (PostViewOutput, PostFetcher)
}

class PostViewControllerFactoryImplementation: PostViewControllerFactory {
    func postViewCoontroller(with loader: Loader, post ID: Int) -> (PostViewOutput, PostFetcher) {
        let imageLoader = NetworkImageLoader(with: loader)
        let cache = CacheImageLoader(with: NSCache<AnyObject, AnyObject>())
        let dataSource = Observable<DataSource<PostCell>>(DataSource(sections: []))
        let compositeConfiguration = PostCellConfigurationFactoryImp().getPostConfiguration(imageLoader: imageLoader, imageCache: cache)
        let postTableView = PostTableViewController(dataSource: dataSource, configurator: compositeConfiguration)
        let presenter = PostPresenter(with: PostLoader(with: loader), view: WeakRef(postTableView))

        return (postTableView, presenter)
    }
}

import UIKit

protocol FeedViewControllerFactory {
    func feedViewCoontroller(with loader: Loader) -> ((FeedViewOutput & FeedViewInput), FeedPresenter)
}

class FeedViewControllerFactoryImplementation: FeedViewControllerFactory {
    func feedViewCoontroller(with loader: Loader) -> ((FeedViewOutput & FeedViewInput), FeedPresenter) {

        let configurator = Configurator<FeedCellView, FeedViewModel> { (cell, viewModel, table, indexPath) -> FeedCellView in
            cell.expandTouched = { [weak table] in
                table?.beginUpdates()
                table?.endUpdates()
            }
            cell.configure(configure: viewModel)

            return cell
        }

        let dataSource = Observable(DataSource<FeedViewModel>(sections: []))
        let feedViewController = FeedViewController(dataSource: dataSource, configurator: configurator)
        let feedPresenter = FeedPresenter(with: FeedLoader(with: loader), view: WeakRef(feedViewController))

        return (feedViewController, feedPresenter)
    }
}

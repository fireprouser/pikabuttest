import UIKit

class AppCoordinator: BaseCoordinator {
    private let window: UIWindow
    private let router: Router
    private let coordinatorFactory: CoordinatorFactory

    init(with window: UIWindow, router: Router, coordinatorFactory: CoordinatorFactory) {
        self.window = window
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }

    override func start() {
        self.window.makeKeyAndVisible()
        self.runFeedFlow()
    }

    func runFeedFlow() {
        var feedCoordinator = coordinatorFactory.getFeedCoordinator(with: self.router)
        feedCoordinator.didEndFlow = {
            self.removeDependency(feedCoordinator)
        }
        self.addDependency(feedCoordinator)
        feedCoordinator.start()
    }
}

protocol Router: Presentable {

  func present(_ module: Presentable?, animated: Bool)

  func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?)

  func popModule(animated: Bool)

  func dismissModule(animated: Bool, completion: (() -> Void)?)

  func setRootModule(_ module: Presentable?, hideBar: Bool)

  func popToRootModule(animated: Bool)

  func popToRootModule(animated: Bool, completion: @escaping () -> ())
}


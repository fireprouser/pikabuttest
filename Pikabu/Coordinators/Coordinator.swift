import UIKit
    
protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set }

    func start()
    func start(with options: DeepLinkOption?)
}

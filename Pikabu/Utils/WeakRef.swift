import Foundation

public final class WeakRef<T: AnyObject> {
    public weak var object: T?

    public init(_ object: T) {
        self.object = object
    }
}


extension WeakRef: FeedView where T: FeedView {
    func showFeed(feed: DataSource<FeedViewModel>) {
        object?.showFeed(feed: feed)
    }
}

extension WeakRef: PostView where T: PostView {
    func showPost(post: DataSource<PostCell>) {
        object?.showPost(post: post)
    }
}


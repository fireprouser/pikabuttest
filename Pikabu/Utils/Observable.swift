import UIKit

class Observable<T> {
    var valueChanged: ((T) -> ())?

    var value: T {
        didSet {
            valueChanged?(self.value)
        }
    }

    init(_ value: T) {
        self.value = value
    }
}

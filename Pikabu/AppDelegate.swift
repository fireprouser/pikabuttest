//
//  AppDelegate.swift
//  Pikabu
//
//  Created by Dmytro on 11/13/19.
//  Copyright © 2019 ReliableSoftwareImp. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var applicationCoordinator: Coordinator?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        let root = UINavigationController()
        self.window?.rootViewController = root

        let coordinator = AppCoordinator(with: window, router: RouterImp(rootController: root), coordinatorFactory: CoordinatorFactoryImp())
        self.applicationCoordinator = coordinator
        coordinator.start()

        let notificationManager = NotificationManager(with: UNUserNotificationCenter.current(), delegate: self)
        notificationManager.requestAuthorization { (granted) in
            print(granted)
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            notificationManager.sendNotification(with: "Please see this post", subtitle: "You want to see it", message: "I really sure") { (error) in
                print(error)
            }
        }

        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)

    {
        let dict = notification.request.content.userInfo as? [String: AnyObject]
        let deepLink = DeepLinkOption.build(with: dict)
        applicationCoordinator?.start(with: deepLink)
    }
}


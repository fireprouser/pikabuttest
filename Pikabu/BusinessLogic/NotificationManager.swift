import UserNotifications
import NotificationCenter
import UserNotificationsUI

class NotificationManager {
    struct Constants {
        static let notificationID = "reliable.softrware.com"
    }

    let center: UNUserNotificationCenter
    let delegate: UNUserNotificationCenterDelegate
    let authOptions: UNAuthorizationOptions = [.alert, .sound]


    init(with userNotificationCenter: UNUserNotificationCenter, delegate: UNUserNotificationCenterDelegate) {
        self.center = userNotificationCenter
        self.delegate = delegate
    }

    func requestAuthorization(completion: @escaping (Bool) -> ()) {
        self.center.requestAuthorization(options: self.authOptions) {[weak self] (granted, error) in
            if error != nil {
                print("We didn't get a notification access")
                return completion(false)
            }

            self?.center.delegate = self?.delegate

            return completion(granted)
        }
    }

    func sendNotification(with title: String, subtitle: String, message: String, completion: ((Error?) -> Void)?) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.subtitle = subtitle
        content.body = message
        content.userInfo[DeepLinkingPostConstants.postID] = 113
        content.userInfo[DeepLinkingCommonStructConstants.type] = DeepLinkingTypeConstants.post

        let request = UNNotificationRequest(identifier: Constants.notificationID, content: content, trigger: nil)
        self.center.add(request, withCompletionHandler: completion)
    }
}

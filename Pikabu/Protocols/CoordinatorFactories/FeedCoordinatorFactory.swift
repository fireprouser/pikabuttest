import Foundation

protocol FeedCoordinatorFactory {
    func getFeedCoordinator(with router: Router) -> (Coordinator & FeedCoordinatorOutput)
}

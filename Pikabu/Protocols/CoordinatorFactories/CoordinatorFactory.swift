import Foundation

protocol CoordinatorFactory: FeedCoordinatorFactory {
}

class CoordinatorFactoryImp: CoordinatorFactory {
    func getFeedCoordinator(with router: Router) -> (Coordinator & FeedCoordinatorOutput) {
        return FeedCoordinator(router: router, feedViewControllerFactory: FeedViewControllerFactoryImplementation(), postViewControllerFactory: PostViewControllerFactoryImplementation())
    }
}

import Foundation

protocol FeedViewOutput: BaseView  {
    var clickOnFeed: ((FeedViewModel)->())? { get set }
}


protocol FeedViewInput: BaseView  {
    var reloadData: (()->())? { get set }
}

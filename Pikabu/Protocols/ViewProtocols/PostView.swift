import Foundation

protocol PostViewOutput: BaseView {
    var viewDidFinished:(() -> ())? { get set }
    var reloadData:(() -> ())? { get set }
}

protocol PostFetcher {
    func fetch(with postID: Int)
}

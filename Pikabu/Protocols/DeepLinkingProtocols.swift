import Foundation

struct DeepLinkingTypeConstants {
    static let post = "post"
}

struct DeepLinkingPostConstants {
    static let postID = "postID"
}

struct DeepLinkingCommonStructConstants {
    static let type = "launch_id"
}

enum DeepLinkOption {
  case post(Int)

  static func build(with dict: [String : AnyObject]?) -> DeepLinkOption? {
    guard let id = dict?[DeepLinkingCommonStructConstants.type] as? String else { return nil }

    guard let itemID = dict?[DeepLinkingPostConstants.postID] as? Int else {
        return nil
    }

    switch id {
      case DeepLinkingTypeConstants.post: return .post(itemID)
      default: return nil
    }
  }
}


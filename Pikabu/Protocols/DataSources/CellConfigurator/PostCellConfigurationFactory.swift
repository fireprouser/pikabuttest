import UIKit

protocol PostCellConfigurationFactory {
    func getPostConfiguration(imageLoader: ImageLoader, imageCache: ImageCache) -> CompositeConfigurator
}


class PostCellConfigurationFactoryImp: PostCellConfigurationFactory {
    func getPostConfiguration(imageLoader: ImageLoader, imageCache: ImageCache) -> CompositeConfigurator {
        return CompositeConfigurator(postCellConfigurator: self.postConfigurator(), imageCellConfigurator: self.imageConfigurator(imageLoader, cache: imageCache))
    }

    private func imageConfigurator(_ imageLoader: ImageLoader, cache: ImageCache) -> Configurator<ImageViewCell, ImageViewModel> {
        return Configurator<ImageViewCell, ImageViewModel> { (cell, viewModel: PostCell.ImageCellViewModel, table, indexPath) -> ImageViewCell in
            let imageURL = viewModel.url

            if let cachedImage = cache.getImage(with: imageURL) {
                viewModel.image.value = cachedImage
            } else {
                imageLoader.loadImage(with: imageURL) { (image) in
                    guard let image = image else {
                        viewModel.image.value = PlaceHolderImage.error.image
                        return
                    }

                    cache.saveImage(with: imageURL, image: image)

                    if imageURL == cell.viewModel?.url {
                        DispatchQueue.main.async {
                            viewModel.image.value = image
                            table.reloadRows(at: [indexPath], with: .none)
                        }
                    }
                }
            }
            cell.setup(with: viewModel)

            return cell
        }
    }

    private func postConfigurator() -> Configurator<PostViewCell, PostViewModel> {
        return Configurator<PostViewCell, PostViewModel> { (cell, viewModel: PostCell.PostCellViewModel, tableView, IndexPath) -> PostViewCell in
            cell.setup(with: viewModel)
            return cell
        }
    }
}

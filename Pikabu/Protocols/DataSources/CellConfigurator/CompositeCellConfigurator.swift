import UIKit

enum PostCell {
    typealias ImageCellViewModel = ImageViewModel
    typealias PostCellViewModel = PostViewModel

    case imageCell(ImageCellViewModel)
    case postCell(PostCellViewModel)
}

struct CompositeConfigurator: CellConfigurator {
    let postCellConfigurator: Configurator<PostViewCell, PostCell.PostCellViewModel>
    let imageCellConfigurator: Configurator<ImageViewCell, PostCell.ImageCellViewModel>

    func configuration(cell: UITableViewCell, with model: PostCell, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch model {
        case .imageCell(let imageModel):
            return self.imageCellConfigurator.cellFor(model: imageModel, indexPath: indexPath, tableView: tableView)
        case .postCell(let postModel):
            return self.postCellConfigurator.cellFor(model: postModel, indexPath: indexPath, tableView: tableView)
        }
    }

    func stringIdentifier(model: PostCell, indexPath: IndexPath) -> String {
        switch model {
        case .imageCell:
            return imageCellConfigurator.reuseIdentifier
        case .postCell:
            return postCellConfigurator.reuseIdentifier
        }
    }

    func registerCells(in tableView: UITableView) {
        postCellConfigurator.registerCells(in: tableView)
        imageCellConfigurator.registerCells(in: tableView)
    }
}

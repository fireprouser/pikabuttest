import UIKit

protocol CellConfigurator {
    associatedtype Cell: UITableViewCell
    associatedtype Model

    func configuration(cell: Cell, with model: Model, tableView: UITableView, indexPath: IndexPath) -> Cell
    func stringIdentifier(model: Model, indexPath: IndexPath) -> String
    func registerCells(in tableView: UITableView)
}

extension CellConfigurator {
    func cellFor(model: Model, indexPath: IndexPath, tableView: UITableView) -> Cell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.stringIdentifier(model: model, indexPath: indexPath)) as! Cell
        return self.configuration(cell: cell, with: model, tableView: tableView, indexPath: indexPath)
    }
}

struct Configurator<Cell: UITableViewCell, Model> : CellConfigurator {
    typealias CellConfigurator = (Cell, Model, UITableView, IndexPath) -> Cell

    let configurator: CellConfigurator
    let reuseIdentifier = "\(Cell.self)"

    func configuration(cell: Cell, with model: Model, tableView: UITableView, indexPath: IndexPath) -> Cell {
        return self.configurator(cell, model, tableView, indexPath)
    }

    func stringIdentifier(model: Model, indexPath: IndexPath) -> String {
        return self.reuseIdentifier
    }

    func registerCells(in tableView: UITableView) {
        if let path = Bundle.main.path(forResource: reuseIdentifier, ofType: "nib"),
            FileManager.default.fileExists(atPath: path) {
            let nib = UINib(nibName: reuseIdentifier, bundle: .main)
            tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(Cell.self, forCellReuseIdentifier: reuseIdentifier)
        }
    }
}

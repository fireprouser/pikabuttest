import Foundation

struct Section<Item> {
    var items: [Item]
}

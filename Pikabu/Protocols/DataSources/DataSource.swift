import Foundation

struct DataSource<T> {
    var sections: [Section<T>]

    func sectionCount() -> Int {
        return sections.count
    }

    func countItems(in section: Int) -> Int {
        return section < sections.count ? self.sections[section].items.count : 0
    }

    func item(for indexPath: IndexPath) -> T {
        return sections[indexPath.section].items[indexPath.row]
    }
}

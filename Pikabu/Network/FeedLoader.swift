import Foundation

protocol FeedProvider {
    func provideFeed(completion: @escaping ([FeedItem]) -> ())
}

class FeedLoader: FeedProvider {
    struct Constant {
        static let feedURL = URL(string: "http://cs.pikabu.ru/files/api201910/posts.json")!
    }

    private let loader: Loader

    init(with loader: Loader) {
        self.loader = loader
    }

    func provideFeed(completion: @escaping ([FeedItem]) -> ()) {
        let resource = Resource<Feed>(get: Constant.feedURL)
        self.loader.load(resourse: resource) { (result, error) in
            print(error)
            completion(result?.posts ?? [])
        }
    }
}

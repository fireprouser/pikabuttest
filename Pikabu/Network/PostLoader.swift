import Foundation

protocol PostProvider {
    func loadPost(with postID: Int, completion: @escaping((PostItem?) -> ()))
}

class PostLoader: PostProvider {
    private struct Constants {
        static let url = URL(string: "http://cs.pikabu.ru/files/api201910/")!
    }

    private let loader: Loader

    init(with loader: Loader) {
        self.loader = loader
    }

    func loadPost(with postID: Int, completion: @escaping ((PostItem?) -> ())) {
        let url = Constants.url.appendingPathComponent("\(postID).json")

        let resource = Resource<Post>(get: url)
        
        self.loader.load(resourse: resource) { (post, error) in
            completion(post?.post)
        }
    }
}

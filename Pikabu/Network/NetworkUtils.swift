import Foundation

enum HTTPMethod<Body> {
    case get
    case post(Body)
}

struct Resource<A> {
    var request: URLRequest
    var parse: (Data) -> A?
}

extension Resource where A: Decodable {
    
    init(get url: URL) {
        self.request = URLRequest(url: url)
        
        self.parse = { data in
            try? JSONDecoder().decode(A.self, from: data)
        }
    }
    
    init<Body: Encodable>(url: URL, method: HTTPMethod<Body>) {
        self.request = URLRequest(url: url)
        
        self.request.httpMethod = method.methodString
        
        switch method {
        case .get: break
        case .post(let body):
            self.request.httpBody = try? JSONEncoder().encode(body)
            self.request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        self.parse = { data in
            try? JSONDecoder().decode(A.self, from: data)
        }
    }
}

extension Resource {
    func map<B> (_ transform: @escaping (A) -> B) -> Resource<B> {
        return Resource<B>(request: self.request) { data in
            self.parse(data).map(transform)
        }
    }
}


extension HTTPMethod {
    var methodString: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        }
    }
}


protocol Loader {
    func load<Result>(resourse: Resource<Result>, complition: @escaping (Result?, NetworkError?)->())
}


extension URLSession: Loader {
    func load<Result>(resourse: Resource<Result>, complition: @escaping (Result?, NetworkError?)->()) {
        
        dataTask(with: resourse.request) { data, response, error in
            if let error = error, let response = response as? HTTPURLResponse {
                let networkError = NetworkError.getNetworkError(from: response, localizableDescription: error.localizedDescription)
                complition(nil, networkError)
                return
            }
            
            complition(data.flatMap { resourse.parse($0) }, nil)
            
        }.resume()
    }
}

import Foundation

enum NetworkError: Error {
    case unknownError(String)
    case connectionError(String)
    case invalidCredentials(String)
    case invalidRequest(String)
    case notFound(String)
    case invalidResponse(String)
    case serverError(String)
    case serverUnavailable(String)
    case timeOut(String)
    case unsuppotedURL(String)
    case unautorized(String)
}

extension NetworkError {
    var localizedDescription: String {
        switch self {
        case .unknownError(let description):
            return description
        case .connectionError(let description):
            return description
        case .invalidCredentials(let description):
            return description
        case .invalidRequest(let description):
            return description
        case .notFound(let description):
            return description
        case .invalidResponse(let description):
            return description
        case .serverError(let description):
            return description
        case .serverUnavailable(let description):
            return description
        case .timeOut(let description):
            return description
        case .unsuppotedURL(let description):
            return description
        case .unautorized(let description):
            return description
        }
    }
}

extension NetworkError {
    static func getNetworkError(from response: HTTPURLResponse, localizableDescription: String) -> NetworkError {
        switch response.statusCode {
        case 401, 403:
            return .unautorized(localizableDescription)
        default:
            return .unknownError(localizableDescription)
        }
    }
}

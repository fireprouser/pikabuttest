import UIKit

class NetworkImageLoader: ImageLoader {
    let loader: Loader
    
    init(with loader: Loader) {
        self.loader = loader
    }
    
    func loadImage(with url: URL, completion: @escaping ((UIImage?) -> ())) {
        var resource = Resource<Data>(get: url)
        resource.parse = { data in
            return data
        }
        self.loader.load(resourse: resource) { (image, error) in
            completion(image.flatMap({UIImage(data: $0)}))
        }
    }
}

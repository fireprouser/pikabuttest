import UIKit

protocol ImageCache {
    func getImage(with key: URL) -> UIImage?
    func saveImage(with key: URL, image: UIImage)
}

class CacheImageLoader: ImageCache {
    let cache: NSCache<AnyObject, AnyObject>

    init(with cache: NSCache<AnyObject, AnyObject>) {
        self.cache = cache
    }

    func getImage(with key: URL) -> UIImage? {
        return self.cache.object(forKey: key as AnyObject) as? UIImage
    }

    func saveImage(with key: URL, image: UIImage) {
        self.cache.setObject(image as AnyObject, forKey: key as AnyObject)
    }

    deinit {
        self.cache.removeAllObjects()
    }
}

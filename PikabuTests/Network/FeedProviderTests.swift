@testable import Pikabu

import XCTest

class FeedProviderTests: XCTestCase {

    func test_provideFeed() {
        let expectation = XCTestExpectation(description: "Wait to load")
        let session = URLSessionMock()
        let feed =  [FeedItem.createFeedItem(), FeedItem.createFeedItem(), FeedItem.createFeedItem()]
        session.data = feed.data()
        let sut = FeedLoader(with: session)

        sut.provideFeed { (items) in
            XCTAssertEqual(items, feed)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
}

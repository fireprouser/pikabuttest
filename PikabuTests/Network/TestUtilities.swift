@testable import Pikabu

import XCTest

class URLSessionDataTaskMock: URLSessionDataTask {

    let closure: () -> ()

    init(with closure: @escaping () -> ()) {
        self.closure = closure
    }

    override func resume() {
        closure()
    }
}

class URLSessionMock: URLSession {
    var data: Data?
    var error: Error?
    var response: URLResponse?

    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {

        return URLSessionDataTaskMock {
            completionHandler(self.data, self.response, self.error)
        }
    }
}

class TestUtils {
    static func loader(with data: Data?, response: URLResponse?, error: Error?) -> Loader {
        let urlSessionMock = URLSessionMock()
        urlSessionMock.data = data
        urlSessionMock.response = response
        urlSessionMock.error = error

        return urlSessionMock
    }
}

extension Encodable {
    var encode: Data {
        return try! JSONEncoder().encode(self)
    }
}

extension FeedItem {
    static func createFeedItem() -> FeedItem {
        return FeedItem(id: 2, timestamp: 5234523452, title: "Лучши посты", preview: "очень осмысленное превью", likesCount: 543)
    }
}

import XCTest

import Foundation

struct Token: Codable, Equatable {
    let token: String

    init(token: String) {
        self.token = token
    }

}

struct User: Codable, Equatable {
    let email: String

    init(email: String) {
        self.email = email
    }
}

import XCTest

@testable import Pikabu

class NetworkUtils: XCTestCase {

    func testCreateGetRequest() {
        let url = URL(string: "http://www.google.com")!
        let resource = Resource<User>(get: url)
        let user = User(email: "dima.rebenko@gmail.com")

        XCTAssertEqual(resource.parse(user.data()), user)
        XCTAssertEqual(resource.request.httpMethod, "GET")
        XCTAssertEqual(resource.request.url, url)
    }

    func testCreateGetRequestWithNilBody() {
        let url = URL(string: "http://www.google.com")!
        let resource = Resource<Token>(url: url, method: HTTPMethod<Token>.get)
        let token = Token(token: "fsdfasdfasd")
        XCTAssertEqual(resource.parse(token.data()), token)
        XCTAssertEqual(resource.request.httpMethod, "GET")
        XCTAssertEqual(resource.request.httpBody, nil)
    }

    func testCreatePostRequest() {
        let url = URL(string: "http://www.google.com")!
        let body = ["user" : "dima", "password" : "pasword"]
        let resource = Resource<Token>(url: url, method: .post(body))
        let token = Token(token: "fsdfasdfasd")
        XCTAssertEqual(resource.parse(token.data()), token)
        XCTAssertEqual(resource.request.httpMethod, "POST")
        XCTAssertEqual(resource.request.httpBody, try! JSONEncoder().encode(body))
    }

    func testMapResource() {
        let url = URL(string: "http://www.google.com")!
        let body = ["user" : "dima", "password" : "pasword"]
        let token = Token(token: "fsdfasdfasd")
        let resource = Resource<Token>(url: url, method: .post(body)).map { (token) -> Token in
            return Token(token: token.token)
        }
        XCTAssertEqual(resource.parse(token.data()), token)
    }

    func testErrorAfterResponse() {
        let expectation = XCTestExpectation(description: "Wait to download")
        let url = URL(string: "http://www.google.com")!
        let body = ["user" : "dima", "password" : "pasword"]
        let token = Token(token: "fsdfasdfasd")
        let resource = Resource<Token>(url: url, method: .post(body)).map { (token) -> Token in
            return Token(token: token.token)
        }

        let urlSessionDataMock = URLSessionMock()
        let response = HTTPURLResponse(url: url, statusCode: 401, httpVersion: nil, headerFields: nil)
        urlSessionDataMock.response = response

        urlSessionDataMock.load(resourse: resource) { (result, error) in
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(resource.parse(token.data()), token)
    }
}

extension Encodable {
    func data() -> Data {
        return try! JSONEncoder().encode(self)
    }
}

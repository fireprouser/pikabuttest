@testable import Pikabu

import XCTest

class FeedProviderSpy: FeedProvider {
    var provideCount: Int = 0

    func provideFeed(completion: @escaping ([FeedItem]) -> ()) {
        provideCount += 1
        completion([FeedItem.createFeedItem(), FeedItem.createFeedItem(), FeedItem.createFeedItem()])
    }
}
